
import ItemListContainer from "./components/itemlistcontainer/ItemListContainer";
import NavBar from "./components/navbar/NavBar";


function App() {
  return (
    
    <>
      
      <NavBar/>
      <ItemListContainer greeting='Proyecto en ReactJS'/>

    </>

  );
}

export default App;
